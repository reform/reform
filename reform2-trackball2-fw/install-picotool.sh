#!/bin/bash

sudo apt install build-essential pkg-config libusb-1.0-0-dev cmake
git clone https://github.com/raspberrypi/pico-sdk
export PICO_SDK_PATH="$(pwd)/pico-sdk"
git clone https://github.com/raspberrypi/picotool
cd picotool
mkdir build
cd build
cmake ..
make -j4
sudo make install
