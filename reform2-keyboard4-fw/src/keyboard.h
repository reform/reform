/*
  SPDX-License-Identifier: GPL-3.0-or-later
  MNT Pocket Reform Keyboard/Trackball Controller Firmware for RP2040
  Copyright 2021-2024 MNT Research GmbH (mntre.com)
*/

#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#define KBD_VARIANT_US 1
#define KBD_VARIANT_INTL 2
#define KBD_VARIANT_NEO2 3

#define KBD_MODE_STANDALONE 1
#define KBD_MODE_LAPTOP 2

//#define KBD_MODE_TRACKPAD 1
//#define KBD_MODE_TRACKBALL 1

void reset_keyboard_state(void);

void led_set_rgb(uint32_t rgb);
void led_set_hsv();
void led_mod_hue(int d);
void led_mod_brightness(int d);
void led_set_brightness(int b);
void led_mod_saturation(int d);
void led_cycle_hue();
int led_get_brightness();

#endif
