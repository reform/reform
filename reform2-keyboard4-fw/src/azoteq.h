/*
  azoteq.h -- Azoteq IQS550 mappings
  Copyright 2023 Valtteri Koskivuori <vkoskiv@gmail.com>
  Copyright 2024 Lukas F. Hartmann / MNT Research GmbH <lukas@mntre.com>
  License: GPLv3
*/

#pragma once

#include <stdint.h>
#include <stdbool.h>

struct azoteq_gesture_events {
  uint8_t ges_unused_0 : 2;
  bool ges_swipe_y_minus : 1;
  bool ges_swipe_y_plus : 1;
  bool ges_swipe_x_plus : 1;
  bool ges_swipe_x_minus : 1;
  bool ges_press_and_hold : 1;
  bool ges_single_tap : 1;

  uint8_t ges_unused_1 : 5;
  bool ges_zoom: 1;
  bool ges_scroll: 1;
  bool ges_2_finger_tap: 1;
} __attribute__((packed));

struct azoteq_sysinfo {
  bool sys_show_reset : 1;
  bool sys_alp_reati_occurred : 1;
  bool sys_alp_ati_error : 1;
  bool sys_reati_occurred : 1;
  bool sys_ati_error : 1;
  uint8_t sys_charging_mode : 3;

  uint8_t sys_unused: 2;
  bool sys_switch_state : 1;
  bool sys_snap_toggle : 1;
  bool sys_rr_missed : 1;
  bool sys_too_many_fingers : 1;
  bool sys_palm_detect : 1;
  bool sys_tp_movement : 1;
} __attribute__((packed));

struct azoteq_header {
  uint16_t product_number;
  uint16_t project_number;
  uint8_t major_version;
  uint8_t minor_version;
  uint8_t bootloader_status;
  uint8_t unused_0[4];
  uint8_t max_touch_column : 4;
  uint8_t max_touch_row : 4;
  uint8_t prev_cycle_time_ms;
  struct azoteq_gesture_events ges_events;
  struct azoteq_sysinfo sys;
} __attribute__((packed));

struct azoteq_finger {
  uint16_t abs_x;
  uint16_t abs_y;
  uint16_t touch_strength;
  uint8_t  touch_area;
} __attribute__((packed));

// Starts at 0x0000, 57 bytes
struct azoteq_data {
  struct azoteq_header header;
  uint8_t num_fingers;
  int16_t relative_x;
  int16_t relative_y;
  struct azoteq_finger fingers[5];
} __attribute__((packed));

#define ADDR_SENSOR_TP 0x74

int read_azoteq_data(struct azoteq_data* data, unsigned int sz);
