/*
  SPDX-License-Identifier: GPL-3.0-or-later
  MNT Pocket Reform Keyboard/Trackball Controller Firmware for RP2040
  Copyright 2021-2024 MNT Research GmbH (mntre.com)
*/

#include "usb_hid_keys.h"
#include "keyboard.h"

#define KBD_COLS 14
#define KBD_ROWS 6
#define KBD_MATRIX_SZ KBD_COLS * KBD_ROWS

#define KEY_HYPER KEY_F23
#define KEY_CIRCLE KEY_F24

// Top row, left to right
#define MATRIX_DEFAULT_ROW_1 \
  KEY_ESC,\
  KEY_F1,\
  KEY_F2,\
  KEY_F3,\
  KEY_F4,\
  KEY_F5,\
  KEY_F6,\
  KEY_F7,\
  KEY_F8,\
  KEY_F9,\
  KEY_F10,\
  KEY_F11,\
  KEY_F12,\
  KEY_CIRCLE

// Second row
#define MATRIX_DEFAULT_ROW_2 \
  KEY_GRAVE,\
  KEY_1,\
  KEY_2,\
  KEY_3,\
  KEY_4,\
  KEY_5,\
  KEY_6,\
  KEY_7,\
  KEY_8,\
  KEY_9,\
  KEY_0,\
  KEY_MINUS,\
  KEY_EQUAL,\
  KEY_BACKSPACE

// Third row
#define MATRIX_DEFAULT_ROW_3 \
  KEY_TAB,\
  KEY_Q,\
  KEY_W,\
  KEY_E,\
  KEY_R,\
  KEY_T,\
  KEY_Y,\
  KEY_U,\
  KEY_I,\
  KEY_O,\
  KEY_P,\
  KEY_LEFTBRACE,\
  KEY_RIGHTBRACE,\
  KEY_BACKSLASH

// Fourth row
#define MATRIX_DEFAULT_ROW_4 \
  KEY_LEFTCTRL,\
  KEY_A,\
  KEY_S,\
  KEY_D,\
  KEY_F,\
  KEY_G,\
  KEY_H,\
  KEY_J,\
  KEY_K,\
  KEY_L,\
  KEY_SEMICOLON,\
  KEY_APOSTROPHE,\
  KEY_ENTER,\
  KEY_ENTER

#if KBD_VARIANT == KBD_VARIANT_INTL
#pragma message "[variant] QWERTY International"
// Fifth row
#define MATRIX_DEFAULT_ROW_5 \
  KEY_LEFTSHIFT,\
  KEY_102ND,\
  KEY_Z,\
  KEY_X,\
  KEY_C,\
  KEY_V,\
  KEY_B,\
  KEY_N,\
  KEY_M,\
  KEY_COMMA,\
  KEY_DOT,\
  KEY_SLASH,\
  KEY_UP,\
  KEY_RIGHTSHIFT
#else
#pragma message "[variant] QWERTY US"
// Fifth row
#define MATRIX_DEFAULT_ROW_5 \
  KEY_LEFTSHIFT,\
  KEY_DELETE,\
  KEY_Z,\
  KEY_X,\
  KEY_C,\
  KEY_V,\
  KEY_B,\
  KEY_N,\
  KEY_M,\
  KEY_COMMA,\
  KEY_DOT,\
  KEY_SLASH,\
  KEY_UP,\
  KEY_RIGHTSHIFT
#endif

// Sixth row
#define MATRIX_DEFAULT_ROW_6 \
  KEY_HYPER,\
  KEY_LEFTMETA,\
  KEY_LEFTALT,\
  KEY_SPACE,\
  KEY_SPACE,\
  KEY_SPACE,\
  KEY_SPACE,\
  KEY_RIGHTALT,\
  KEY_RIGHTCTRL,\
  KEY_LEFT,\
  KEY_DOWN,\
  KEY_RIGHT

// Every line of `matrix` is a row of the keyboard, starting from the top.
// Check keyboard.h for the definitions of the default rows.
uint8_t matrix[KBD_MATRIX_SZ] = {
  MATRIX_DEFAULT_ROW_1,
  MATRIX_DEFAULT_ROW_2,
  MATRIX_DEFAULT_ROW_3,
  MATRIX_DEFAULT_ROW_4,
  MATRIX_DEFAULT_ROW_5,
  MATRIX_DEFAULT_ROW_6
};

// When holding down HYPER
uint8_t matrix_fn[KBD_MATRIX_SZ] = {
  // TODO Media keys on Hyper + F7-F12
  KEY_SYSRQ,
  0x6f,
  0x70,
  KEY_F3,
  KEY_F4,
  KEY_F5,
  KEY_F6,
  KEY_MEDIA_BACK,
  KEY_MEDIA_PLAYPAUSE,
  KEY_MEDIA_FORWARD,
  KEY_MEDIA_MUTE,
  KEY_MEDIA_VOLUMEDOWN,
  KEY_MEDIA_VOLUMEUP,
  KEY_CIRCLE,

  MATRIX_DEFAULT_ROW_2,
  MATRIX_DEFAULT_ROW_3,
  MATRIX_DEFAULT_ROW_4,

  KEY_LEFTSHIFT,
#if KBD_VARIANT == KBD_VARIANT_INTL
  KEY_102ND,
#else
  KEY_DELETE,
#endif
  KEY_Z,
  KEY_X,
  KEY_C,
  KEY_V,
  KEY_B,
  KEY_N,
  KEY_M,
  KEY_COMMA,
  KEY_DOT,
  KEY_SLASH,
  KEY_PAGEUP,
  KEY_RIGHTSHIFT,

  KEY_HYPER,
  KEY_LEFTMETA,
  KEY_LEFTALT,
  KEY_SPACE,
  KEY_SPACE,
  KEY_SPACE,
  KEY_SPACE,
  KEY_RIGHTALT,
  KEY_RIGHTCTRL,
  KEY_HOME,
  KEY_PAGEDOWN,
  KEY_END
};
