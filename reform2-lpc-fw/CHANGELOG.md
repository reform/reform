### 2024-10-15

 - add charger soft-start for motherboard 2.5; alleviates charger brick shutdown issue in most situations

### 2024-07-26

 - new LPC commands: f, 1f, 2f, 3f, and U to report version strings and uptime
 - write out descriptive error if `REFORM_MOTHERBOARD_REV` is missing (during build)

### 2023-11-24

 - disable brownout reset during sleep to keep battery status

### 2023-07-03

 - first tagged version
