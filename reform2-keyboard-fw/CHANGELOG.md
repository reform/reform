### 2024-10-15

 - adopt new USB vendor/product ID from https://pid.codes/1209

### 2024-07-26

 - use common, minimized LUFA sources

### 2023-11-24

 - allow usb hid commands to draw on all 128 display columns
 - v3: add home/end/pgup/pgdn on hyper layer
 - fade-in & fade-out animations for keyboard backlight
 - add a ~30s timeout to the MNT Reform logo on the OLED to prevent burn-in
 - mention on the OLED that firmware update mode is active

### 2023-07-03

 - first tagged version
